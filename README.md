# The Great Places app

Application developed in Sections 12, 13 and 14 of Udemy's "React Native - The Practical Guide" course by Maximilian Scharzmuller

## Section 12: Native Device Features (Camera, Maps, Location, SQLite)

### Environment variables

In Lecture 237, we created a basic environment variables file _(env.js)_.

This basic file just exports a JS object - but you could get more fancy and for example export different environment variables for your development flow (i.e. for testing/ developing your app) and for production (i.e. for when you publish your app).

The special **DEV** global variable offered by Expo helps you - it's a variable which you can always access anywhere in your Expo-driven React Native project to determine whether you're running this app in development mode or not.

Therefore, you could create a more elaborate environment variables file like this one:

_env.js_

```js
const variables = {
  development: {
    googleApiKey: "abc"
  },
  production: {
    googleApiKey: "xyz"
  }
};
const getEnvVariables = () => {
  if (__DEV__) {
    return variables.development; // return this if in development mode
  }
  return variables.production; // otherwise, return this
};

export default getEnvVariables; // export a reference to the function
```

You would use that file like this:

_someOtherFile.js_

```js
import ENV from './env';
...
const apiKey = ENV().googleApiKey;
```

### Useful links

- [SQLite Example](https://snack.expo.io/@git/github.com/expo/sqlite-example)

- [Expo Native Modules Docs](https://docs.expo.io/versions/latest/) (check "API Reference")

## Section 13: Building Apps Without Expo

### Ejecting from Expo's Managed Workflow

Run `expo eject` and select the bare option in the CLI. This will over write the existing project and create a react-native-CLI kind of project. Once ejected, the project can't be turned back to expo managed workflow.

Expo packages can still be used with the help of React Unimodules
